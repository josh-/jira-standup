import {
  BoardsResponse,
  isIssuesResponse,
  isBoardsResponse,
} from "./types/response";
import { Project } from "./types/project";
import { Board } from "./types/board";
import { Issue } from "./types/issue";

export default class {
  async getProjects(): Promise<Project[] | null> {
    return this.get("/rest/api/latest/project");
  }

  async getBoards(): Promise<Board[] | null> {
    const response = await this.get<BoardsResponse>("/rest/agile/1.0/board");
    if (!response) {
      return null;
    }

    return response.values;
  }

  async getIssues(boardId: string): Promise<Issue[] | null> {
    const params = new URLSearchParams({ expand: "changelog" });

    const issues = (await this.getPaged(
      `/rest/agile/1.0/board/${boardId}/issue`,
      params
    )) as Issue[] | null;
    if (!issues || issues.length === 0) {
      return null;
    }

    return issues;
  }

  async get<T>(url: string): Promise<T | null> {
    try {
      const response = await AP.request(url);
      return JSON.parse(response.body) as T;
    } catch (e) {
      return null;
    }
  }

  async getPaged(
    url: string,
    params: URLSearchParams
  ): Promise<(Board | Issue)[] | null> {
    let items: (Board | Issue)[] = [];

    let startAt = 0;
    const maxResults = 50;

    try {
      while (true) {
        params.set("startAt", startAt.toString());
        params.set("maxResults", maxResults.toString());
        const response = await AP.request(`${url}?${params.toString()}`);

        const parsed = JSON.parse(response.body);
        if (isIssuesResponse(parsed)) {
          const issues = parsed.issues;
          if (issues.length === 0) {
            break;
          } else {
            items = items.concat(issues);
          }
        } else if (isBoardsResponse(parsed)) {
          const values = parsed.values;
          if (values.length === 0) {
            break;
          } else {
            items = items.concat(values);
          }
        }

        startAt += maxResults;

        if (parsed.total < startAt) {
          break;
        }
      }

      return items;
    } catch (e) {
      return null;
    }
  }
}
