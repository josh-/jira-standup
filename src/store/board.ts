import { createStore, createHook } from "react-sweet-state";
import API from "../api";
import { Board } from "../types/board";

interface StoreState {
  loadingBoards: boolean;
  boards: Board[] | null;
}

const store = createStore({
  initialState: {
    loadingBoards: false,
    boards: new Array<Board>(),
  } as StoreState,
  actions: {
    loadBoards: () => async ({ setState, getState }) => {
      if (getState().loadingBoards === true) {
        return;
      }

      setState({
        loadingBoards: true,
      });

      const api = new API();

      const boards = await api.getBoards();

      setState({
        loadingBoards: false,
        boards,
      });
    },
  },
});

const useBoards = createHook(store);
export { useBoards };
