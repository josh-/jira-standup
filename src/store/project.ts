import { createStore, createHook } from "react-sweet-state";
import API from "../api";
import { Project } from "../types/project";

interface StoreState {
  loadingProjects: boolean;
  projects: Project[] | null;
}

const store = createStore({
  initialState: {
    loadingProjects: false,
    projects: new Array<Project>(),
  } as StoreState,
  actions: {
    loadProjects: () => async ({ setState, getState }) => {
      if (getState().loadingProjects === true) {
        return;
      }

      setState({
        loadingProjects: true,
      });

      const api = new API();

      const projects = await api.getProjects();

      setState({
        loadingProjects: false,
        projects,
      });
    },
  },
});

const useProjects = createHook(store);
export { useProjects };
