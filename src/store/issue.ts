import { createStore, createHook } from "react-sweet-state";
import API from "../api";
import { Issue } from "../types/issue";

interface StoreState {
  loadingIssues: boolean;
  issues: Issue[] | null;
}

const store = createStore({
  initialState: {
    loadingIssues: false,
    issues: new Array<Issue>(),
  } as StoreState,
  actions: {
    loadIssues: (boardId: string) => async ({ setState, getState }) => {
      if (getState().loadingIssues === true) {
        return;
      }

      setState({
        loadingIssues: true,
      });

      const api = new API();

      const issues = await api.getIssues(boardId);

      setState({
        loadingIssues: false,
        issues,
      });
    },
  },
});

const useIssues = createHook(store);
export { useIssues };
