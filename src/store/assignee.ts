import { createStore, createHook } from "react-sweet-state";
import API from "../api";
import { Issue } from "../types/issue";
import { Assignee } from "../types/assignee";
import { lastBusinessDay, sameDay } from "../helpers/dates";
import { shuffleArray } from "../helpers/shuffleArray";

interface StoreState {
  loadingAssignees: boolean;
  assignees: Assignee[] | null;
  currentAssigneeIndex: number;
}

const store = createStore({
  initialState: {
    loadingAssignees: false,
    assignees: new Array<Assignee>(),
    currentAssigneeIndex: 0,
  } as StoreState,
  actions: {
    incrementCurrentAssignee: () => ({ setState, getState }) => {
      setState({
        currentAssigneeIndex: getState().currentAssigneeIndex + 1,
      });
    },

    decrementCurrentAssignee: () => ({ setState, getState }) => {
      const current = getState().currentAssigneeIndex;
      if (current === 0) {
        return;
      }

      setState({
        currentAssigneeIndex: current - 1,
      });
    },

    resetCurrentAssignee: () => ({ setState }) => {
      setState({
        currentAssigneeIndex: 0,
      });
    },

    loadAssignees: (boardId: string) => async ({ setState, getState }) => {
      if (getState().loadingAssignees === true) {
        return;
      }

      setState({
        loadingAssignees: true,
      });

      const api = new API();

      const issues = await api.getIssues(boardId);

      let assignees = issues?.reduce<Assignee[]>(
        (accumulator: Assignee[], currentValue: Issue) => {
          const { assignee } = currentValue.fields;
          if (assignee === null) {
            return accumulator;
          }

          if (assignee.active === false) {
            return accumulator;
          }

          if (currentValue.fields.resolution == null) {
            const { accountId } = assignee;
            const index = accumulator.findIndex((a) => a.id === accountId);
            if (index > -1) {
              accumulator[index].issues?.current?.push(currentValue);
            } else {
              const { displayName, avatarUrls } = assignee;

              accumulator.push({
                id: accountId,
                name: displayName,
                avatarUrls: avatarUrls,
                issues: {
                  current: [currentValue],
                },
              });
            }
          }

          const { changelog } = currentValue;
          if (changelog?.histories) {
            for (const item of changelog?.histories) {
              const occurredOnLastBusinessDay = sameDay(
                lastBusinessDay(),
                new Date(item.created)
              );
              if (!occurredOnLastBusinessDay) {
                break;
              }

              const { author } = item;
              const authorAccountId = author.accountId;
              const authorIndex = accumulator.findIndex(
                (a) => a.id === authorAccountId
              );
              if (authorIndex > -1) {
                // Check if the current issue is already in history
                const issueId = currentValue.id;
                const historyIssueIndex = accumulator[
                  authorIndex
                ].issues.history?.findIndex((i) => i.id === issueId);

                if (historyIssueIndex !== undefined && historyIssueIndex > -1) {
                  // Issue is in history
                  // This ts-ignore is needed because the compiler isn't able to determine whether the `history` object is valid with the array index
                  // @ts-ignore
                  accumulator[authorIndex].issues.history[
                    historyIssueIndex
                  ].changelog?.histories.push(item);
                } else {
                  // Issue is not in history
                  const copiedCurrentIssue = currentValue;

                  if (copiedCurrentIssue.changelog) {
                    copiedCurrentIssue.changelog.histories = [item];
                  }

                  accumulator[authorIndex].issues.history?.push(
                    copiedCurrentIssue
                  );
                }
              } else {
                // Author of changelog item is not already in history

                let newAssignee = {
                  name: item.author.displayName,
                  id: item.author.accountId,
                  avatarUrls: item.author.avatarUrls,
                  issues: {
                    current: [],
                    history: [],
                  },
                };
                const newLength = accumulator.push(newAssignee);
                const newAuthorIndex = newLength - 1;

                const copiedCurrentIssue = currentValue;
                if (copiedCurrentIssue.changelog) {
                  copiedCurrentIssue.changelog.histories = [item];
                }

                accumulator[newAuthorIndex].issues.history?.push(
                  copiedCurrentIssue
                );
              }
            }
          }

          return accumulator;
        },
        []
      );

      if (assignees !== undefined) {
        assignees = shuffleArray(assignees);
      }

      setState({
        loadingAssignees: false,
        assignees,
      });
    },
  },
});

const useAssignees = createHook(store);
export { useAssignees };
