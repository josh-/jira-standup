import { createStore, createHook } from "react-sweet-state";

const store = createStore({
  initialState: {
    standupStartDate: new Date(),
    timeWarningShown: false,
    timeAlertShown: false,
  },
  actions: {
    setStandupStartDate: () => ({ setState }) => {
      setState({
        standupStartDate: new Date(),
      });
    },
    setTimeWarningShown: () => ({ setState }) => {
      setState({
        timeWarningShown: true,
      });
    },
    setTimeAlertShown: () => ({ setState }) => {
      setState({
        timeAlertShown: true,
      });
    },
  },
});

const useTimer = createHook(store);
export { useTimer };
