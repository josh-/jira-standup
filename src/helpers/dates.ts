export const lastBusinessDay = (): Date => {
  const date = new Date();
  date.setDate(date.getDate() - 1);

  const day = date.getDay();
  if (day === 0) {
    // Sunday
    date.setDate(date.getDate() - 2);
  } else if (day === 6) {
    // Saturday
    date.setDate(date.getDate() - 1);
  }

  return date;
};

export const sameDay = (firstDate: Date, secondDate: Date): boolean => {
  return (
    firstDate.getFullYear() === secondDate.getFullYear() &&
    firstDate.getMonth() === secondDate.getMonth() &&
    firstDate.getDay() === secondDate.getDay()
  );
};
