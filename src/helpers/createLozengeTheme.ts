import { ThemeProp } from "@atlaskit/theme";
import { ThemeTokens, ThemeAppearance } from "@atlaskit/lozenge/dist/cjs/theme";

interface ThemeProps {
  appearance: ThemeAppearance | Record<string, any>;
  isBold: boolean;
  maxWidth: number | string;
}

export const createLozengeTheme = (
  backgroundColor: string,
  textColor: string
): ThemeProp<ThemeTokens, ThemeProps> => {
  return (theme, props) => {
    return {
      ...theme(props),
      backgroundColor,
      textColor,
    };
  };
};
