import { StatusCategoryColor } from "../types/issue";

export const statusCategoryColorToLozengeColors = (
  color: StatusCategoryColor
): { backgroundColor: string; textColor: string } => {
  switch (color) {
    case "medium-gray":
      return { backgroundColor: "#ccc", textColor: "#333" };
    case "green":
      return { backgroundColor: "#14892c", textColor: "#fff" };
    case "yellow":
      return { backgroundColor: "#ffd351", textColor: "#594300" };
    case "brown":
      return { backgroundColor: "#815b3a", textColor: "#fff" };
    case "warm-red":
      return { backgroundColor: "#d04437", textColor: "#fff" };
    case "blue-gray":
      return { backgroundColor: "#4a6785", textColor: "#fff" };
    default:
      throw new Error("Invalid color");
  }
};
