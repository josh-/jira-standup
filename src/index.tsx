import React from "react";
import ReactDOM from "react-dom";
import { ErrorBoundary } from "./components/ErrorBoundary";
import App from "./components/App";
import "./index.css";

ReactDOM.render(
  <React.StrictMode>
    <ErrorBoundary>
      <App />
    </ErrorBoundary>
  </React.StrictMode>,
  document.getElementById("root")
);
