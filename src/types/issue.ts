import { PagedResults } from "./response";

export type StatusCategoryColor =
  | "medium-gray"
  | "green"
  | "yellow"
  | "brown"
  | "warm-red"
  | "blue-gray";

export interface Issue {
  /**
   * Comma-delimited list
   */
  changelog?: ChangelogResult;
  expand: string;
  fields: {
    aggregateprogress: {
      progress: number;
      total: number;
    };
    aggregatetimeestimate: any | null;
    aggregatetimeoriginalestimate: any | null;
    aggregatetimespent: any | null;
    assignee: {
      accountId: string;
      accountType: "atlassian" | string;
      active: boolean;
      avatarUrls: {
        "16x16": string;
        "24x24": string;
        "32x32": string;
        "48x48": string;
      } | null;
      displayName: string;
      /**
       * Full URL
       */
      self: string;
      timeZone: string;
    };
    attachment: any[];
    comments: {
      comments: Array<{
        self: string;
        id: string;
        author: {
          self: string;
          accountId: string;
          emailAddress: string;
          avatarUrls: {
            "48x48": string;
            "24x24": string;
            "16x16": string;
            "32x32": string;
          };
          displayName: string;
          active: boolean;
          timeZone: string;
          accountType: "atlassian" | string;
        };
        body: string;
        updateAuthor: {
          self: string;
          accountId: string;
          emailAddress: string;
          avatarUrls: {
            "48x48": string;
            "24x24": string;
            "16x16": string;
            "32x32": string;
          };
          displayName: string;
          active: boolean;
          timeZone: string;
          accountType: "atlassian" | string;
        };
        /**
         * ISO8601 date
         */
        created: string;
        /**
         * ISO8601 date
         */
        updated: string;
        jsdPublic: boolean;
      }>;
      maxResults: number;
      startAt: number;
      total: number;
    };
    components: any[];
    /**
     * ISO8601 date
     */
    created: string;
    creator: {
      accountId: string;
      accountType: "atlassian" | string;
      active: boolean;
      avatarUrls: {
        "48x48": string;
        "24x24": string;
        "16x16": string;
        "32x32": string;
      };
      displayName: string;
      emailAddress: string;
      /**
       * Full URL
       */
      self: string;
      timeZone: string;
    };
    customfield_10000: string | object | null;
    customfield_10001: string | object | null;
    customfield_10002: string | object | null;
    customfield_10003: string | object | null;
    customfield_10004: string | object | null;
    customfield_10005: string | object | null;
    customfield_10006: string | object | null;
    customfield_10007: string | object | null;
    customfield_10008: string | object | null;
    customfield_10009: string | object | null;
    customfield_10010: string | object | null;
    customfield_10014: string | object | null;
    customfield_10015: string | object | null;
    customfield_10016: string | object | null;
    customfield_10017: string | object | null;
    customfield_10018: string | object | null;
    customfield_10019: string | object | null;
    customfield_10020: string | object | null;
    customfield_10021: string | object | null;
    customfield_10022: string | object | null;
    customfield_10023: string | object | null;
    customfield_10026: string | object | null;
    description: string | null;
    duedate: string | null;
    environment: string | object | null;
    epic: string | object | null;
    fixVersions: any[];
    flagged: boolean;
    issuelinks: any[];
    issuetype: {
      avatarId: number;
      description: string;
      /**
       * Full URL
       */
      iconUrl: string;
      id: string;
      name: string;
      /**
       * Full URL
       */
      self: string;
      subtask: boolean;
    };
    labels: any[];
    lastViewed: string;
    priority: {
      /**
       * Full URL
       */
      iconUrl: string;
      id: string;
      name: "Highest" | "Higher" | "Medium" | "Lower" | "Lowest" | string;
      /**
       * Full API URL
       */
      self: string;
      progress: {
        progress: number;
        total: number;
      };
    };
    project: {
      avatarUrls: {
        "16x16": string;
        "24x24": string;
        "32x32": string;
        "48x48": string;
      };
      id: string;
      key: string;
      name: string;
      projectTypeKey: "software" | "business" | "service_desk";
      /**
       * Full API URL
       */
      self: string;
      simplified: boolean;
    };
    reporter: {
      accountId: string;
      accountType: "atlassian" | string;
      active: boolean;
      avatarUrls: {
        "16x16": string;
        "24x24": string;
        "32x32": string;
        "48x48": string;
      };
      displayName: string;
      emailAddress: string;
      /**
       * Full API URL
       */
      self: string;
      timeZone: string;
    };
    resolution: any | null;
    resolutiondate: any | null;
    security: any | null;
    sprint: any | null;
    status: {
      description: string;
      /**
       * Full URL
       */
      iconUrl: string;
      id: string;
      /**
       * Example: "Backlog"
       */
      name: string;
      /**
       * Full API URL
       */
      self: string;
      statusCategory: {
        /**
         * Example: "blue-gray"
         */
        colorName: StatusCategoryColor;
        id: number;
        /**
         * Example: "new"
         */
        key: string;
        /**
         * Example: "To Do"
         */
        name: string;
        /**
         * Full API URL
         */
        self: "https://joshp.atlassian.net/rest/api/2/statuscategory/2";
      };
    };
    /**
     * ISO8601 date
     */
    statuscategorychangedate: string;
    subtasks: any[];
    /**
     * Example: "Build backend"
     */
    summary: string;
    timeestimate: any | null;
    timeoriginalestimate: any | null;
    timespent: any | null;
    timetracking: object;
    /**
     * ISO8601 date
     */
    updated: string;
    versions: any[];
    votes: {
      hasVoted: boolean;
      /**
       * Full API URL
       */
      self: string;
      votes: number;
    };
    watches: {
      isWatching: boolean;
      /**
       * Full API URL
       */
      self: string;
      watchCount: number;
    };
    worklog: {
      maxResults: number;
      startAt: number;
      total: number;
      worklogs: any[];
    };
    workratio: number;
  };
  id: string;
  key: string;
  /**
   * Full URL
   */
  self: string;
  /**
   * ISO8601 date
   */
}

export interface ChangelogResult extends PagedResults {
  histories: Changelog[];
}

export interface Changelog {
  author: {
    accountId: string;
    accountType: "atlassian" | string;
    active: boolean;
    avatarUrls: {
      "16x16": string;
      "24x24": string;
      "32x32": string;
      "48x48": string;
    };
    displayName: string;
    emailAddress: string;
    /**
     * Full API URL
     */
    self: string;
    timeZone: string;
  };
  created: string;
  id: string;
  items: Array<{
    /**
     * Example: "assignee"
     */
    field: string;
    /**
     * Example: "assignee"
     */
    fieldId: string;
    /**
     * Example: "jira"
     */
    fieldtype: string;
    /**
     * Example: "assignee"
     */
    from: string | null;
    fromString: string | null;
    tmpFromAccountId?: string | null;
    tmpToAccountId?: string | null;
    to: string;
    toString: string;
  }>;
}
