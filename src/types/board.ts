export interface Board {
  id: number;
  location: {
    /**
     * Relative URL
     */
    avatarURI: string;
    displayName: string;
    name: string;
    projectId: number;
    projectKey: string;
    projectName: string;
    projectTypeKey: "software" | "business" | "service_desk";
  };
  name: string;
  /**
   * Full URL
   */
  self: string;
  type: "kanban" | "scrum" | "simple";
}
