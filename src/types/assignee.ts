import { Issue, Changelog } from "./issue";

export interface Assignee {
  name: string;
  id: string;
  avatarUrls: {
    "16x16": string;
    "24x24": string;
    "32x32": string;
    "48x48": string;
  } | null;
  issues: {
    current?: Array<Issue> | null;
    history?: Array<Issue> | null;
  };
}
