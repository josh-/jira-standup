export interface Project {
  /**
   * Comma-delimited list
   */
  expand: "description,lead,issueTypes,url,projectKeys,permissions,insight";
  /**
   * Full URL
   */
  self: string;
  id: string;
  key: string;
  name: string;
  avatarUrls: {
    "48x48": string;
    "24x24": string;
    "16x16": string;
    "32x32": string;
  };
  projectTypeKey: "software" | "business" | "service_desk";
  simplified: boolean;
  style: "classic" | "next-gen";
  isPrivate: boolean;
  properties: object;
}
