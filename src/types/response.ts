import { Issue } from "./issue";
import { Board } from "./board";

export interface PagedResults {
  maxResults: number;
  startAt: number;
  total: number;
}

export interface BoardsResponse extends PagedResults {
  isLast: boolean;
  values: Board[];
}

export interface IssuesResponse extends PagedResults {
  /**
   * Comma-delimited list
   */
  expand: string;
  issues: Issue[];
}

// Runtime type guards

const isBoardsResponse = (response: object): response is BoardsResponse => {
  return (response as BoardsResponse).values !== undefined;
};

const isIssuesResponse = (response: object): response is IssuesResponse => {
  return (response as IssuesResponse).issues !== undefined;
};

export { isBoardsResponse, isIssuesResponse };
