import React from "react";
import Page from "@atlaskit/page";
import Home from "./Home";

function App() {
  return (
    <div>
      <Page>
        <Home />
      </Page>
    </div>
  );
}

export default App;
