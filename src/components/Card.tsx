import React from "react";
import Lozenge from "@atlaskit/lozenge";
import { statusCategoryColorToLozengeColors } from "../helpers/statusColor";
import { StatusCategoryColor } from "../types/issue";
import { createLozengeTheme } from "../helpers/createLozengeTheme";
import styled from "styled-components";

const CardContainer = styled.div`
  padding: 24px 10px;
  box-shadow: rgba(9, 30, 66, 0.25) 0px 1px 1px,
    rgba(9, 30, 66, 0.31) 0px 0px 1px;
`;

const CardRow = styled.div`
  display: flex;
  justify-content: space-between;
`;

const HistoryRow = styled.div``;

const Summary = styled.span`
  font-weight: 600;
`;

const Id = styled.span`
  font-size: 14px;
`;

const ImageTextContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const TextContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-left: 10px;
`;

const StatusContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

interface CardProps {
  iconUrl: string;
  issueType: string;
  id: string;
  summary: string;
  statusName: string;
  statusColor: StatusCategoryColor;
  children?: JSX.Element;
}

export default ({
  iconUrl,
  issueType,
  id,
  summary,
  statusName,
  statusColor,
  children,
}: CardProps) => {
  const { backgroundColor, textColor } = statusCategoryColorToLozengeColors(
    statusColor
  );
  const theme = createLozengeTheme(backgroundColor, textColor);

  return (
    <CardContainer>
      <CardRow>
        <ImageTextContainer>
          <img src={iconUrl} title={issueType} alt={issueType} />
          <TextContainer>
            <Summary>{summary}</Summary>
            <Id>{id}</Id>
          </TextContainer>
        </ImageTextContainer>
        <StatusContainer>
          <Lozenge theme={theme}>{statusName}</Lozenge>
        </StatusContainer>
      </CardRow>
      <HistoryRow>{children}</HistoryRow>
    </CardContainer>
  );
};
