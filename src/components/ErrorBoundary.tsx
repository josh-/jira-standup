import React from "react";
import Error from "@atlaskit/icon/glyph/error";
import { colors } from "@atlaskit/theme";
import Page from "@atlaskit/page";
import EmptyState from "@atlaskit/empty-state";
import { Button } from "@atlaskit/button/dist/cjs/components/Button";
import styled from "styled-components";

interface ErrorBoundaryProps {
  hasError: boolean;
}

const IconContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 3em;
`;

const ErrorPage = () => (
  <Page>
    <IconContainer>
      <Error label="Error icon" primaryColor={colors.R300} size="xlarge" />
    </IconContainer>
    <EmptyState
      header="Sorry, something went wrong."
      description="Please try again, or contact support."
      primaryAction={
        <Button
          appearance="primary"
          onClick={() => (window.location.href = "/")}
        >
          Reload
        </Button>
      }
    />
  </Page>
);

export class ErrorBoundary extends React.Component<{}, ErrorBoundaryProps> {
  state = {
    hasError: false,
  };

  static getDerivedStateFromError() {
    return { hasError: true };
  }

  render() {
    const { hasError } = this.state;
    if (hasError) {
      return <ErrorPage />;
    }

    return this.props.children;
  }
}
