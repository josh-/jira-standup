import React, { useState, useEffect } from "react";
import Flag, { FlagGroup, FlagProps } from "@atlaskit/flag";
import { useTimer } from "../store/timer";
import styled from "styled-components";
import { ActionsType } from "@atlaskit/flag/dist/cjs/types";
import Warning from "@atlaskit/icon/glyph/warning";
import Error from "@atlaskit/icon/glyph/error";
import { colors } from "@atlaskit/theme";

const ONE_SECOND = 1000;
const ONE_MINUTE = ONE_SECOND * 60;

const SubtleText = styled.span`
  color: #7a869a;
`;

const WarningText = styled.span`
  color: #ffab00;
`;

const AlertText = styled.span`
  color: #bf2600;
`;

const minutesDifference = (firstDate: Date, secondDate: Date) => {
  return Math.round((firstDate.getTime() - secondDate.getTime()) / ONE_MINUTE);
};

const timerComponent = (difference: number) => {
  const minutesText = difference === 1 ? "min" : "mins";

  if (difference < 12) {
    return (
      <SubtleText>
        {difference} {minutesText}
      </SubtleText>
    );
  } else if (difference > 12 && difference < 15) {
    return (
      <WarningText>
        {difference} {minutesText}
      </WarningText>
    );
  } else {
    return (
      <AlertText>
        {difference} {minutesText}
      </AlertText>
    );
  }
};

export const Timer = () => {
  const [
    { standupStartDate, timeWarningShown, timeAlertShown },
    { setTimeWarningShown, setTimeAlertShown },
  ] = useTimer();

  const [currentDate, setCurrentDate] = useState(new Date());
  const tick = () => {
    setCurrentDate(new Date());
  };
  useEffect(() => {
    const intervalID = setInterval(() => tick(), ONE_MINUTE);
    return () => {
      clearInterval(intervalID);
    };
  });

  const [flags, setFlags] = useState<FlagProps[]>([]);
  if (minutesDifference(currentDate, standupStartDate) === 12) {
    if (!timeWarningShown) {
      const newFlags = flags;
      newFlags.unshift({
        id: 1,
        icon: <Warning label="Warning icon" primaryColor={colors.Y300} />,
        title: "Approaching 15 minutes",
        description: "Keep it short and sharp!",
        appearance: "warning",
      });
      setFlags(newFlags);
      setTimeWarningShown();
    }
  } else if (minutesDifference(currentDate, standupStartDate) === 15) {
    if (!timeAlertShown) {
      const newFlags = flags;
      newFlags.unshift({
        id: 2,
        icon: <Error label="Error icon" primaryColor={colors.R300} />,
        title: "Maybe take it offline?",
        description: "Try to limit your standups to 15 minutes.",
        appearance: "error",
      });
      setFlags(newFlags);
      setTimeAlertShown();
    }
  }

  const actions: ActionsType = [
    {
      content: "Dismiss",
      onClick: () => setFlags(flags.slice(1)),
    },
  ];

  return (
    <>
      {timerComponent(minutesDifference(currentDate, standupStartDate))}
      <FlagGroup>
        {flags.map((flag) => (
          <Flag actions={actions} {...flag} />
        ))}
      </FlagGroup>
    </>
  );
};
