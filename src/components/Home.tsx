import React, { useEffect, useState, useRef } from "react";
import Button from "@atlaskit/button";
import Spinner from "@atlaskit/spinner";
import PageHeader from "@atlaskit/page-header";
import Modal, {
  HeaderComponentProps,
  FooterComponentProps,
  ModalFooter,
  ModalTransition,
  KeyboardOrMouseEvent,
} from "@atlaskit/modal-dialog";
import { Grid, GridColumn } from "@atlaskit/page";
import EmptyState from "@atlaskit/empty-state";
import styled from "styled-components";
import { useBoards } from "../store/board";
import ContentWrapper from "./ContentWrapper";
import ModalContent from "./ModalContent";
import { Timer } from "./Timer";
import { useTimer } from "../store/timer";
import { useAssignees } from "../store/assignee";
import { Board } from "../types/board";
import { Tip } from "./Tip";
import Avatar from "@atlaskit/avatar";

const ProjectCard = styled.div`
  border-radius: 3px;
  box-shadow: rgba(9, 30, 66, 0.25) 0px 1px 1px,
    rgba(9, 30, 66, 0.31) 0px 0px 1px;
  display: flex;
  flex-direction: column;
  padding: 10px;
  align-items: center;
`;

const Center = styled.div`
  display: flex;
  justify-content: space-around;
`;

const ProjectAvatar = styled.img`
  border-radius: 3px;
  max-width: 64px;
`;

const BoardsContainer = styled.div`
  width: 100%;
`;

const BoardsHeading = styled.h5`
  text-transform: uppercase;
  margin: 0;
`;

const ButtonSpacing = styled.div`
  margin: 8px 0;
`;

export default () => {
  const [{ loadingBoards, boards }, { loadBoards }] = useBoards();
  useEffect(() => {
    loadBoards();
  }, [loadBoards]);

  const [, { setStandupStartDate }] = useTimer();
  const [, { resetCurrentAssignee }] = useAssignees();

  const [selectedBoardId, setSelectedBoardId] = useState<string | null>(null);
  const [dialogOpen, setDialogOpen] = useState(false);

  const boardsGroupedByProject = boards?.reduce<
    Array<{
      projectId: number;
      projectName: string;
      self: string;
      avatarURI: string;
      boards: Array<Board>;
    }>
  >((results, board) => {
    const {
      location: { projectId, projectName, avatarURI },
      self,
    } = board;

    const index = results.findIndex((b) => b.projectId === projectId);
    if (index > -1) {
      results[index].boards.push(board);
    } else {
      results.push({
        projectId,
        projectName,
        self,
        avatarURI,
        boards: [board],
      });
    }

    return results;
  }, []);

  return (
    <ContentWrapper>
      <PageHeader>Standups</PageHeader>
      <Tip />
      {loadingBoards && (
        <Center>
          <Spinner size="xlarge" />
        </Center>
      )}
      {loadingBoards === false && (
        <>
          <p>Select a board below to start a standup:</p>
          <Grid>
            {boardsGroupedByProject &&
              boardsGroupedByProject.length > 0 &&
              boardsGroupedByProject.map((project) => (
                <GridColumn medium={3}>
                  <ProjectCard>
                    <ProjectAvatar
                      src={`${new URL(project.self).origin}${
                        project.avatarURI
                      }`}
                    />
                    <h3>{project.projectName}</h3>
                    <BoardsContainer>
                      <BoardsHeading>Boards</BoardsHeading>
                      {project.boards.map((board) => (
                        <ButtonSpacing>
                          <Button
                            appearance="default"
                            onClick={() => {
                              setStandupStartDate();
                              setSelectedBoardId(board.id.toString());
                              setDialogOpen(true);
                            }}
                          >
                            {board.name}
                          </Button>
                        </ButtonSpacing>
                      ))}
                    </BoardsContainer>
                  </ProjectCard>
                </GridColumn>
              ))}
            {boardsGroupedByProject && boardsGroupedByProject.length === 0 && (
              <EmptyState
                header="No JIRA boards"
                description="You don't have access to any JIRA boards. To run a standup either create a board, or ask a team mate to add you to a board."
                primaryAction={
                  <Button
                    appearance="primary"
                    onClick={() => (window.location.href = "/")}
                  >
                    Reload
                  </Button>
                }
              ></EmptyState>
            )}
          </Grid>
        </>
      )}
      {dialogOpen && (
        <ModalTransition>
          <Modal
            shouldCloseOnEscapePress
            width="x-large"
            height="100%"
            components={{
              Header,
              Footer,
            }}
            onClose={() => {
              resetCurrentAssignee();
              setDialogOpen(false);
            }}
          >
            {selectedBoardId && (
              <ModalContent
                boardId={selectedBoardId}
                closeModal={() => setDialogOpen(false)}
              />
            )}
          </Modal>
        </ModalTransition>
      )}
    </ContentWrapper>
  );
};

const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 24px 24px 6px;
`;

const HeaderText = styled.div`
  font-size: 20px;
  font-weight: 500;
`;

const AvatarContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const AvatarName = styled.span`
  margin-left: 10px;
`;

const Header: React.FunctionComponent<HeaderComponentProps> = () => {
  const [
    { loadingAssignees, assignees, currentAssigneeIndex },
  ] = useAssignees();

  return (
    <HeaderContainer>
      <HeaderText>Standup</HeaderText>
      {loadingAssignees === false &&
        assignees?.length !== currentAssigneeIndex && (
          <AvatarContainer>
            {assignees && (
              <Avatar
                src={assignees[currentAssigneeIndex].avatarUrls?.["48x48"]}
              />
            )}
            <AvatarName>
              {assignees && assignees[currentAssigneeIndex].name}
            </AvatarName>
          </AvatarContainer>
        )}
      <Timer />
    </HeaderContainer>
  );
};

const Footer: React.FunctionComponent<FooterComponentProps> = ({ onClose }) => {
  const [
    { loadingAssignees, assignees, currentAssigneeIndex },
    { incrementCurrentAssignee, decrementCurrentAssignee },
  ] = useAssignees();

  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const keyUpHandler = (event: KeyboardEvent) => {
      const { key } = event;
      switch (key) {
        case "ArrowLeft":
          if (currentAssigneeIndex !== 0) {
            decrementCurrentAssignee();
          }
          break;
        case "ArrowRight":
          if (currentAssigneeIndex === assignees?.length) {
            onClose((event as unknown) as KeyboardOrMouseEvent);
          } else {
            incrementCurrentAssignee();
          }
          break;
        default:
          break;
      }
    };

    if (ref.current !== null) {
      ref.current.focus();
    }
    document.addEventListener("keyup", keyUpHandler);
    return () => {
      document.removeEventListener("keyup", keyUpHandler);
    };
  }, [
    assignees,
    currentAssigneeIndex,
    decrementCurrentAssignee,
    incrementCurrentAssignee,
    onClose,
  ]);

  return (
    <>
      {loadingAssignees === false && (
        <ModalFooter tabIndex={-1} ref={ref} style={{ outline: "none" }}>
          <Button
            isDisabled={currentAssigneeIndex === 0}
            onClick={() => decrementCurrentAssignee()}
          >
            Back
          </Button>
          {currentAssigneeIndex !== assignees?.length && (
            <Button
              appearance="primary"
              onClick={() => incrementCurrentAssignee()}
            >
              Next
            </Button>
          )}
          {currentAssigneeIndex === assignees?.length && (
            <Button appearance="primary" onClick={onClose}>
              Finish
            </Button>
          )}
        </ModalFooter>
      )}
    </>
  );
};
