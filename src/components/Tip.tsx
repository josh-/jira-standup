import React from "react";
import styled from "styled-components";
import InlineMessage from "@atlaskit/inline-message";

const tips = [
  "Choose a standup time that works for everyone, it doesn't have to be in the morning",
  "Standups work best when they are 15 minutes or less",
  "Keep everyone engaged during standup",
  "Talk about your standups during your retrospective, and make changes if things aren't working!",
];

const randomTip = (): string => tips[Math.floor(Math.random() * tips.length)];

const TipContainer = styled.div`
  margin: "1em 0";
`;

export const Tip = () => (
  <>
    <TipContainer>
      <InlineMessage
        type="info"
        title={randomTip()}
        secondaryText="Find out more"
      >
        <p>
          This recommendation comes from{" "}
          <a
            href="https://www.atlassian.com/agile/scrum/standups"
            target="_blank"
            rel="noopener noreferrer"
          >
            Atlassian's Agile Coach guide
          </a>
          .
        </p>
      </InlineMessage>
    </TipContainer>
  </>
);
