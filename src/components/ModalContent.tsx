import React, { useEffect } from "react";
import styled from "styled-components";
import Spinner from "@atlaskit/spinner";
import Card from "./Card";
import { Done } from "./Done";
import { useAssignees } from "../store/assignee";
import { lastBusinessDay } from "../helpers/dates";
import Lozenge from "@atlaskit/lozenge";
import { colors } from "@atlaskit/theme";

interface ModalContentProps {
  boardId: string;
  closeModal: () => void;
}

const SpinnerContainer = styled.div`
  display: flex;
  justify-content: space-around;
  margin: 5em 0;
`;

const SectionHeader = styled.h5`
  text-transform: uppercase;
  margin: 20px 0 10px;
`;

const Timeline = styled.ul`
  margin-block-end: 0;
  list-style-type: none;
`;

const BlackCircle = "\\25CF";

const TimelineItem = styled.li`
  position: relative;
  margin: 0px;
  padding-top: 2px;
  padding-bottom: 2px;
  padding-left: 20px;

  &:before {
    content: "";
    background-color: ${colors.N700};
    position: absolute;
    bottom: 0px;
    top: 0px;
    left: 5px;
    width: 2px;
  }

  &:after {
    content: " ${BlackCircle}";
    font-size: 11px;
    left: 0px;
    color: ${colors.N700};
    position: absolute;
    text-align: center;
    padding-top: 3px;
    padding-left: 1px;
  }
`;

export default ({ boardId, closeModal }: ModalContentProps) => {
  const [
    { loadingAssignees, assignees, currentAssigneeIndex },
    { loadAssignees },
  ] = useAssignees();
  useEffect(() => {
    loadAssignees(boardId);
  }, [loadAssignees, boardId]);

  const previousWorkDayName = Intl.DateTimeFormat(undefined, {
    weekday: "long",
  }).format(lastBusinessDay());

  return (
    <>
      {loadingAssignees && (
        <SpinnerContainer>
          <Spinner size="xlarge" />
        </SpinnerContainer>
      )}
      {loadingAssignees === false &&
        (assignees?.length === currentAssigneeIndex ? (
          <Done />
        ) : (
          <>
            {assignees &&
              assignees[currentAssigneeIndex].issues.history?.length && (
                <SectionHeader>On {previousWorkDayName}</SectionHeader>
              )}

            {assignees &&
              assignees[currentAssigneeIndex].issues.history?.map(
                (historyIssue) => (
                  <>
                    <Card
                      iconUrl={historyIssue.fields.issuetype.iconUrl}
                      issueType={historyIssue.fields.issuetype.name}
                      id={historyIssue.key}
                      summary={historyIssue.fields.summary}
                      statusName={historyIssue.fields.status.name}
                      statusColor={
                        historyIssue.fields.status.statusCategory.colorName
                      }
                    >
                      <Timeline>
                        {historyIssue.changelog?.histories
                          .map((h) => h.items)
                          .flat()
                          .map((item) => {
                            if (item) {
                              const timelineEntry = formatTimelineEntry(
                                item.field,
                                item.fromString,
                                item.toString
                              );
                              if (timelineEntry) {
                                return (
                                  <TimelineItem>{timelineEntry}</TimelineItem>
                                );
                              }
                            }
                          })}
                      </Timeline>
                    </Card>
                  </>
                )
              )}

            <SectionHeader>Today</SectionHeader>
            {assignees &&
              assignees[currentAssigneeIndex].issues?.current?.map((i) => (
                <Card
                  iconUrl={i.fields.issuetype.iconUrl}
                  issueType={i.fields.issuetype.name}
                  id={i.key}
                  summary={i.fields.summary}
                  statusName={i.fields.status.name}
                  statusColor={i.fields.status.statusCategory.colorName}
                ></Card>
              ))}
          </>
        ))}
    </>
  );
};

const TimelineText = styled.span`
  font-size: 14px;
`;

const formatTimelineEntry = (
  field: string,
  from: string | null,
  to: string | null
) => {
  if (field === "status") {
    if (from != null) {
      return (
        <TimelineText>
          Moved from <Lozenge>{from}</Lozenge> to <Lozenge>{to}</Lozenge>
        </TimelineText>
      );
    } else {
      return (
        <TimelineText>
          Moved to <Lozenge>{to}</Lozenge>
        </TimelineText>
      );
    }
  } else if (field === "labels") {
    return (
      <TimelineText>
        Add label to <Lozenge>{to}</Lozenge>
      </TimelineText>
    );
  } else if (field === "Link") {
    return (
      <TimelineText>
        Added link <Lozenge>{to}</Lozenge>
      </TimelineText>
    );
  } else if (field === "assignee") {
    if (from != null) {
      return (
        <TimelineText>
          Changed assignee from <Lozenge>{from}</Lozenge> to{" "}
          <Lozenge>{to}</Lozenge>
        </TimelineText>
      );
    } else {
      return (
        <TimelineText>
          Set assignee to <Lozenge>{to}</Lozenge>
        </TimelineText>
      );
    }
  } else if (field === "resolution") {
    if (to != null) {
      return (
        <TimelineText>
          Set resolution to <Lozenge>{to}</Lozenge>
        </TimelineText>
      );
    } else {
      return (
        <TimelineText>
          Removed resolution <Lozenge>{from}</Lozenge>
        </TimelineText>
      );
    }
  }
};
