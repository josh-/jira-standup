import React from "react";
import { Grid, GridColumn } from "@atlaskit/page";

export default ({ children }: { children: any }) => (
  <Grid>
    <GridColumn>{children}</GridColumn>
  </Grid>
);
