import React from "react";
import styled from "styled-components";

const randomEmoji = (): string => {
  const emojis = ["🎉", "🙌", "💫", "✨", "⚡️", "💥", "👊", "👏", "😺", "🥳"];
  const index = Math.floor(Math.random() * emojis.length);
  return emojis[index];
};

const DoneContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const DoneText = styled.p`
  font-size: 60px;
`;

export const Done = () => {
  return (
    <DoneContainer>
      <h1>Standup done</h1>
      <DoneText>{randomEmoji()}</DoneText>
    </DoneContainer>
  );
};
