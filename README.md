# JIRA Standup

Better standups.

## Installing in your JIRA account

1. Go to JIRA Settings > Manage Apps
2. Select "Upload app"
3. Enter `https://jira-standup.s3.amazonaws.com/atlassian-connnect.json`
4. Click Upload
5. In the JIRA navigation, go to Apps > Standup

## Running locally with `ngrok`

1. From anywhere run `ngrok http 8000`
2. Copy the `https://` URL and set the `baseUrl` property in `atlassian-connect.json` to be this URL
3. Run `yarn build`
4. In the `build` directory run `http-server -p 8000` (requires `http-server` to be installed globally with Node)
